# node-tips
A collection of hopefully useful tips and general node information.

## Cache

If you add the below lines to your ~/.npmrc file 'npm install' is much faster since it uses cache first.  

cache=packages

cache-min=99999999
